var faker = require('faker');
var fs = require('fs');
var jsonServer = require('json-server');
var server = jsonServer.create();

createObjects();
function createObjects () {
	
	var arrObjects = [];
	
	for(var i = 0; i < 200; i++) {			

		var objNew = {
			name: faker.name.findName(),							
			ukCountry: faker.address.country(),
			brState: faker.address.state(),
			zipCode: faker.address.zipCode(),	
			city: faker.address.city(),
			streetName: faker.address.streetName(),
			phoneNumber: faker.phone.phoneNumber(),
			email: faker.internet.email(),	
			ip: faker.internet.ip(),
			userName: faker.internet.userName(),
			companyName: faker.company.companyName(),
			avatar: faker.image.avatar(),
			business: faker.image.business(),
			cityImage: faker.image.city()
		};
		arrObjects.push(objNew);
	}	
	write(JSON.stringify(arrObjects, null, 2));		
}

function write (listJson) {	
	var objJson = '{\n \"exemplo\": ' + listJson + '\n}';
	var strFileName = 'exemplo.json';
	var dirName = __dirname + '/' + strFileName; 	
	      		
	fs.writeFile(dirName, objJson, 'utf-8', function (err) {
    if (err) throw err;

    initServer(strFileName);
    console.log('Arquivo escrito com sucesso');  	
    
  });		
}

function initServer (fileName) {
	var router = jsonServer.router(fileName);
	var middlewares = jsonServer.defaults();

	server.use(middlewares);
	server.use(router);
	var port = 3000;
	
	server.listen(port, function () {
	  console.log('App executando na porta '+ port);
	});
}